# Streams Message Broker

Event-driven microservices using Redis streams.

## Local Dev

```
chmod +x run.sh
./run.sh
```

Inside `run.sh`:
```
export PORT=8000

# build/ directory ignored by git
go build -o build/api .

build/api
```
