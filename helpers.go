package broker

import (
	"fmt"

	"github.com/go-redis/redis/v8"
)

func InitRedis() {
	// default to dev redis instance
	if redisHost == "" {
		redisHost = "127.0.0.1"
		fmt.Println("Env var nil, using redis dev address -- " + redisHost)
	}
	if redisPort == "" {
		redisPort = "6379"
		fmt.Println("Env var nil, using redis dev port -- " + redisPort)
	}
	fmt.Println("Connecting to Redis on " + redisHost + ":" + redisPort)
	rdb = redis.NewClient(&redis.Options{
		Addr: redisHost + ":" + redisPort,
	})
}

//TODO: modify this function to take a lambda that executes on every key/value in msg
func FindInStreamMsg(streamResp []string, key string) string {
	var data string
	readKey := false
	for _, r := range streamResp {
		if r == key {
			readKey = true
		} else if readKey {
			readKey = false
			data = r
		}
	}
	return data
}
