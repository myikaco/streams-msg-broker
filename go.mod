module gitlab.com/myikaco/streams-msg-broker

go 1.16

require (
	github.com/go-redis/redis/v8 v8.5.0
)
