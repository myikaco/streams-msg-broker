package broker

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
)

func StartStream() {
	tempStreamName := "userEvents"
	var ctxStrat = context.Background()

	for {
		newID, err := rdb.XAdd(ctxStrat, &redis.XAddArgs{
			Stream: tempStreamName,
			Values: []string{
				"newEvent",
				time.Now().Local().String(),
			},
		}).Result()
		if err != nil {
			log.Fatal("XADD error -- ", err.Error())
		}

		l, xlenErr := rdb.Do(ctxStrat, "XLEN", tempStreamName).Result()
		if xlenErr != nil {
			log.Fatal("XLEN error -- ", xlenErr.Error())
		}

		if newID != "" {
			fmt.Print("Added to stream " + newID + " / len = ")
			fmt.Println(l)
		}

		time.Sleep(1500 * time.Millisecond)

		if l.(int64) > 10 {
			fmt.Println("Resetting DB with flushall...")
			rdb.Do(ctxStrat, "flushall").Result()
		}
	}
}

//returns last ID and response from XREAD
func ListenStream(streamName string, lastID string) (string, []string) {
	var ret []string
	var retLastID string = ""
	var ctx = context.Background()

	//keep XREAD until got new response on passed stream
	for {
		streams, err := rdb.XRead(ctx, &redis.XReadArgs{
			Streams: []string{streamName, lastID},
			Block:   500,
		}).Result()

		if err != nil {
			fmt.Println("XREAD error -- ", err.Error())
			fmt.Println("Sleeping 5s before retry...")
			time.Sleep(5000 * time.Millisecond)
		} else {
			retLastID = streams[len(streams)-1].Messages[0].ID
			//for each message, add each key-value pair to return array
			for _, stream := range streams[0].Messages {
				for key, val := range stream.Values {
					if val != "" {
						ret = append(ret, fmt.Sprintf("Msg %s: %s = %s", stream.ID, key, val)) //use a map instead
					}
				}
			}
			//stop listening once got some result
			break
		}
	}
	return retLastID, ret
}
